/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
 


import React, { useCallback, useEffect, useMemo, useReducer, useRef, useState } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';


import Welcome from './components/Welcome';
import Mex from './components/Mex';
import Apps from './src/app';

import ClassCounter from './src/ClassCounter';
import HookCounter from './src/HookCounter';

export default function App(): React.JSX.Element {
  var a =2;
  const b = 3;
  function add(){
    var c =  4;
    console.log('Hello', a, b, c);
    
  }
  add();
  
  const addr = () => {
    var d = 5;
    console.log('Hi', a , b, d);
  }
  addr();
  
 // useState()
 // useCallback()
 // useRef
 // useMemo
 // useReducer
 // useEffect
 // lifecycle of class component

  useEffect(()=>{

    return () =>{
      
    }
  },[])
  useEffect(()=>{

    return () =>{
      
    }
  },[])
  useEffect(()=>{

    return () =>{
      
    }
  },[])
  

  return (
    
    <View>
      <Text>
        Hello Dello
      </Text>
      <Welcome />
      <Mex></Mex>


      <ClassCounter />
      <HookCounter />
    </View>
    
  );
}


