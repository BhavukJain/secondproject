// rce

import React, { Component } from 'react'

import { View, Button, Text, StyleSheet } from 'react-native';


class ClassCounter extends Component {
    constructor(props) { // rconst
      super(props);
    
      this.state = {
         count : 0,
      };
    }

    incrementCount = () => {
        this.setState({
            count: this.state.count + 1
        });
    };
    
    render() {
        return (
          <View style={styles.container}>
            <Text style={styles.text}>Count {this.state.count}</Text>
            <Button onPress={this.incrementCount} title="Increment Count" />
          </View>
        );
  }
}

const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
    },
    text: {
      fontSize: 18,
      marginBottom: 10,
    },
  });
  
  export default ClassCounter;