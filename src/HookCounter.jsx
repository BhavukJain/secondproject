import React , {useState} from 'react'
import { View, Button, Text, StyleSheet } from 'react-native';


function HookCounter() {
    const[count, setCount] = useState(0);
    return (
        <View style={styles.container}>
          <Text style={styles.text}>Count: {count}</Text>
          <Button title="Increment Count" onPress={() => setCount(count + 1)} />
        </View>
      );
    }
    
    const styles = StyleSheet.create({
      container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
      },
      text: {
        fontSize: 18,
        marginBottom: 10,
      },
    });
    
    export default HookCounter;
/*
// this code is in react
import React , {useState} from 'react'


function HookCounter() {
    const[count, setCount] = useState(0)
  return (
    <View>
        <Button onPress={() => setCount(count+1)}>Count + {count}</Button>
    </View>
  )
}

export default HookCounter
*/